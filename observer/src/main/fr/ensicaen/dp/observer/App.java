package fr.ensicaen.dp.observer;

import fr.ensicaen.dp.observer.implementation.Bar;
import fr.ensicaen.dp.observer.implementation.DataSource;
import fr.ensicaen.dp.observer.implementation.PieChart;
import fr.ensicaen.dp.observer.implementation.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static Logger logger = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {

		DataSource manager = new DataSource();
		manager.addObserver(new Text());
		manager.addObserver(new PieChart());
		manager.addObserver(new Bar());

		logger.info("\n ***** New State = 5   ***** \n");
		manager.setNewState(5);
		
		logger.info("\n ***** New State = 10  ***** \n");
		manager.setNewState(10);

	}

}
