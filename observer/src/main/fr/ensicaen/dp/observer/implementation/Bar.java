package fr.ensicaen.dp.observer.implementation;

import fr.ensicaen.dp.observer.abstraction.Observer;
import fr.ensicaen.dp.observer.abstraction.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bar implements Observer {

	private static Logger logger = LoggerFactory.getLogger(Bar.class);

	public void update(Subject subject) {
		logger.info("Bar updates... \t\t-> " + subject.getState());
	}

}
