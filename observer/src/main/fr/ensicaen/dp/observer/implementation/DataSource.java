package fr.ensicaen.dp.observer.implementation;

import java.util.ArrayList;

import fr.ensicaen.dp.observer.abstraction.Observer;
import fr.ensicaen.dp.observer.abstraction.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataSource implements Subject {

	private static Logger logger = LoggerFactory.getLogger(DataSource.class);


	ArrayList<Observer> representations;
	private int state; 
	
	public DataSource(){
		representations = new ArrayList<Observer>();
		state = 0;
	}

	public void addObserver(Observer o) {
		representations.add(o);
	}

	public void removeObserver(Observer o) {
		representations.remove(o);
	}

	public void notifyObserver() {
		for (Observer r : representations) {
			r.update(this);
		}		
	}

	public int getState() {
		return state;
	}
	
	public void setNewState(int value) {
		state = value;
		notifyObserver();
	}

}
