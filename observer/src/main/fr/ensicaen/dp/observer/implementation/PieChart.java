package fr.ensicaen.dp.observer.implementation;

import fr.ensicaen.dp.observer.abstraction.Observer;
import fr.ensicaen.dp.observer.abstraction.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PieChart implements Observer {

	private static Logger logger = LoggerFactory.getLogger(DataSource.class);

	public void update(Subject subject) {
		logger.info("PieChart updates... \t-> " + subject.getState());
	}

}
