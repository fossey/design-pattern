package fr.ensicaen.dp.observer.abstraction;

public interface Observer {
	
	void update(Subject subject);

}
