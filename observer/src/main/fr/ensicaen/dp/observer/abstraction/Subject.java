package fr.ensicaen.dp.observer.abstraction;

public interface Subject {
	
	void addObserver(Observer o);
	void removeObserver(Observer o);
	void notifyObserver();
	int getState();

}
