package fr.ensicaen.dp.iterator.implementation;

import fr.ensicaen.dp.iterator.abstraction.Iterator;

public class SchoolYearStudentIterator implements Iterator<Student> {
	
	private int index;
	private SchoolYear promotion;
	
	SchoolYearStudentIterator(SchoolYear p){
		this.index = 0;
		this.promotion = p;
	}

	public void next() {
		index++;
	}

	public boolean hasNext() {
		return index+1 <= promotion.getStudents().size();
	}

	public Student currentItem() {
		return promotion.getStudents().get(index);
	}

	public void reset() {
		index =	0;	
	}

}
