package fr.ensicaen.dp.iterator.implementation;

import java.util.ArrayList;
import java.util.List;

import fr.ensicaen.dp.iterator.abstraction.Iterable;
import fr.ensicaen.dp.iterator.abstraction.Iterator;

public class SchoolYear implements Iterable {
	
	private ArrayList<Student> students;
	
	public SchoolYear(){
		students = new ArrayList<Student>();
	}

	public Iterator<Student> getIterator() {
		return new SchoolYearStudentIterator(this);
	}
	
	public List<Student> getStudents(){
		return this.students;
	}

	public void addStudent(Student e){
		this.students.add(e);
	}

}
