package fr.ensicaen.dp.iterator.implementation;

public class Student {
	
	private String name;
	private String firstName;

	
	public Student(String firstName, String name) {
		this.name = name;
		this.firstName = firstName;
	}
	
	public String getNom() {
		return name;
	}
	public String getPrenom() {
		return firstName;
	}
	
	@Override
	public String toString(){
		return firstName + " " + name;
	}

}
