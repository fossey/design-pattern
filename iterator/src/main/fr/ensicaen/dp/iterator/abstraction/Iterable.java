package fr.ensicaen.dp.iterator.abstraction;

public interface Iterable<E> {

	Iterator<E> getIterator();
	
}
