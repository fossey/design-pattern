package fr.ensicaen.dp.iterator.abstraction;

public interface Iterator <E>{
	
	void reset();
	void next();
	boolean hasNext();
	E currentItem();

}
