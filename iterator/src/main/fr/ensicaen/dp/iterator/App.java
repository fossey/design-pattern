package fr.ensicaen.dp.iterator;

import fr.ensicaen.dp.iterator.abstraction.Iterator;
import fr.ensicaen.dp.iterator.implementation.SchoolYear;
import fr.ensicaen.dp.iterator.implementation.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static Logger logger = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {

		SchoolYear p = new SchoolYear();
		
		p.addStudent(new Student("Paul", "Vérot"));
		p.addStudent(new Student("Nicolas", "Sebille"));
		p.addStudent(new Student("Jean Baptiste", "Gomond"));
		p.addStudent(new Student("Abdesamad", "Zianou"));
		
		Iterator<Student> studentIterator1 = p.getIterator();
		Iterator<Student> studentIterator2 = p.getIterator();
		
		studentIterator2.next();
		
		for (int i = 0; i < 3; i++) {
			logger.info("Iterator N°1 -> {}", studentIterator1.currentItem());
			logger.info("Iterator N°2 -> {}", studentIterator2.currentItem());
			logger.info("");
			if(studentIterator1.hasNext()) studentIterator1.next();
			if(studentIterator2.hasNext()) studentIterator2.next();
		}
		
		studentIterator1.reset();
		studentIterator2.reset();
		
		while(studentIterator1.hasNext() && studentIterator2.hasNext()){
			logger.info("Iterator N°1 -> {}", studentIterator1.currentItem());
			logger.info("Iterator N°2 -> {}", studentIterator2.currentItem());
			studentIterator1.next();
			studentIterator2.next();
		}
		
	}

}
