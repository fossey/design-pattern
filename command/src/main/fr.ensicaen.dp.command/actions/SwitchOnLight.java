package fr.ensicaen.dp.command.actions;

import fr.ensicaen.dp.command.abstraction.APIFunction;
import fr.ensicaen.dp.command.objects.Light;

public class SwitchOnLight implements APIFunction {
	
	private Light light;
	
	public SwitchOnLight(Light light) {
		this.light = light;
	}

	public void action() {
		light.switchOn();
	}

	public void undo() {
		light.switchOff();
	}

}
