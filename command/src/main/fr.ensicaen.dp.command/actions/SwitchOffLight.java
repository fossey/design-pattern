package fr.ensicaen.dp.command.actions;

import fr.ensicaen.dp.command.abstraction.APIFunction;
import fr.ensicaen.dp.command.objects.Light;

public class SwitchOffLight implements APIFunction {
	
	private Light light;
	
	public SwitchOffLight(Light light) {
		this.light = light;
	}

	public void action() {
		light.switchOff();
	}

	public void undo() {
		light.switchOn();
	}

}
