package fr.ensicaen.dp.command;

import fr.ensicaen.dp.command.abstraction.FunctionCallManager;
import fr.ensicaen.dp.command.actions.SwitchOffLight;
import fr.ensicaen.dp.command.actions.SwitchOnLight;
import fr.ensicaen.dp.command.objects.Button;
import fr.ensicaen.dp.command.objects.Light;

public class App {

	public static void main(String[] args) {
		
		Light red = new Light();
		FunctionCallManager manager = new FunctionCallManager();
		
		Button on = new Button(new SwitchOnLight(red));
		Button off = new Button(new SwitchOffLight(red));
		
		on.action(manager);
		off.action(manager);
		
		manager.executeNextCommand();
		manager.executeNextCommand();
		
	}

}
