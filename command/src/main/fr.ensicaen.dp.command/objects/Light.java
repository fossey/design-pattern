package fr.ensicaen.dp.command.objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Light {

	private static Logger logger = LoggerFactory.getLogger(Light.class);
	
	public void switchOn(){
		logger.info("ON");
	}
	
	public void switchOff(){
		logger.info("OFF");
	}
	
}
