package fr.ensicaen.dp.command.objects;

import fr.ensicaen.dp.command.abstraction.APIFunction;
import fr.ensicaen.dp.command.abstraction.FunctionCallManager;

public class Button {

	private APIFunction function;

	public Button(APIFunction f){
		function = f;
	}
	
	public void action(FunctionCallManager manager){
		manager.send(function);
	}
	
}
