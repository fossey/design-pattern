package fr.ensicaen.dp.command.abstraction;

public interface APIFunction {
	
	void action();
	void undo();
}
