package fr.ensicaen.dp.command.abstraction;

import java.util.ArrayList;

public class FunctionCallManager {
	
	private ArrayList<APIFunction> stack;
	
	public FunctionCallManager(){
		stack = new ArrayList<APIFunction>();
	}
	
	public void send(APIFunction f){
		stack.add(f);
	}
	
	public void executeNextCommand(){
		stack.remove(0).action();
	}

}
