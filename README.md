# Design Pattern

### Build
```shell
> mvn clean install
```


### Run target maven from parent module

_Run strategy application_
```shell
> mvn exec:java -pl <artifactId>
```