package fr.ensicaen.dp.singleton.implementation;

public class MemoryManager {
	
	private static MemoryManager manager = null;
	
	private MemoryManager(){}
	
	public static MemoryManager getInstance(){
		if(manager == null){
			manager = new MemoryManager();
		}
		
		return manager;
	}
		
}
