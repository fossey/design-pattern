package fr.ensicaen.dp.singleton;

import fr.ensicaen.dp.singleton.implementation.MemoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args){

        MemoryManager memoryManager = MemoryManager.getInstance();

        logger.info("It's the same object : {} .", memoryManager.equals(MemoryManager.getInstance()));

    }
}
