package fr.ensicaen.dp.state.implementation.states;

import java.util.ArrayList;
import java.util.List;

import fr.ensicaen.dp.state.abstraction.Context;
import fr.ensicaen.dp.state.abstraction.State;
import fr.ensicaen.dp.state.implementation.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaitPassword implements State {

	private static Logger logger = LoggerFactory.getLogger(WaitPassword.class);
    private static String ACCESS_DENIED = "Acces denied !";
    private static String STATE = "Wait password !";

	private String username;

	WaitPassword(String username){
		this.username = username;
		logger.info(this.toString());
	}

	public void user(Context c, String username) {
		logger.info("Name already set");
	}

	public void pass(Context c, String password) {
		c.setState(new Connected(username,password));
	}

	public int list(Context c, int messageNumber) {
		logger.info(ACCESS_DENIED);
		return -1;
	}

	public Mail retrieve(Context c, int messageNumber) {
		logger.info(ACCESS_DENIED);
		return null;
	}
	
	public List<Mail> retrieve(Context c) {
		logger.info(ACCESS_DENIED);
		return new ArrayList<Mail>();
	}

	public void quit(Context c) {
		c.setState(new WaitUsername());
	}
	
	public String toString(){
		return STATE;
	}

}
