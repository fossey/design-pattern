package fr.ensicaen.dp.state.implementation;

public class Mail {
		
	private String content;
	
	Mail(String content){
		this.content = content;
	}
	
	public String getContent(){
		return content;
	}
	
	public int getSize(){
		return this.content.length();
	}
	
	@Override
	public String toString(){
		return ">>>\n\tSize : " + this.getSize() + "\n\tContent : " + this.content + "\n>>>";
	}

}
