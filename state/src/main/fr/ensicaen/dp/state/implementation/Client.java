package fr.ensicaen.dp.state.implementation;

import fr.ensicaen.dp.state.abstraction.Context;
import fr.ensicaen.dp.state.abstraction.State;
import fr.ensicaen.dp.state.implementation.states.WaitUsername;

import java.util.List;

public class Client implements Context {

	private State state;
	
	public Client(){
		state = new WaitUsername();
	}
	
	public void setState(State s) {
		this.state = s;
	}

	public void user(String username) {
		state.user(this, username);		
	}

	public void pass(String password) {
		state.pass(this, password);
	}

	public int list(int messageNumber) {
		return state.list(this, messageNumber);
	}

	public List<Mail> retrieve() {
		return this.state.retrieve(this);
	}

	public Mail retrieve(int messageNumber) {
		return this.state.retrieve(this, messageNumber);
	}

	public void quit() {
		this.state.quit(this);
	}


}
