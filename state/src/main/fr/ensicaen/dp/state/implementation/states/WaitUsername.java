package fr.ensicaen.dp.state.implementation.states;

import java.util.ArrayList;
import java.util.List;

import fr.ensicaen.dp.state.abstraction.Context;
import fr.ensicaen.dp.state.abstraction.State;
import fr.ensicaen.dp.state.implementation.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaitUsername implements State {

    private static String ACCESS_DENIED = "Acces denied !";

	private static Logger logger = LoggerFactory.getLogger(WaitUsername.class);
	
	public WaitUsername(){
		logger.info(this.toString());
	}

	public void user(Context c, String username) {
		c.setState(new WaitPassword(username));
	}

	public void pass(Context c, String password) {
		logger.info("Insert username before");
	}

	public int list(Context c, int messageNumber) {
		logger.info(ACCESS_DENIED);
		return -1;
	}

	public Mail retrieve(Context c, int messageNumber) {
		logger.info(ACCESS_DENIED);
		return null;
	}

	public List<Mail> retrieve(Context c) {
		logger.info(ACCESS_DENIED);
		return new ArrayList<Mail>();
	}
	
	public void quit(Context c) {
		logger.info("Already disconneted");
	}
	
	public String toString(){
		return "Disconnected - username waiting !";
	}
}
