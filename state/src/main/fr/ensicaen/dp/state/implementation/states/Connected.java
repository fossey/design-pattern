package fr.ensicaen.dp.state.implementation.states;

import fr.ensicaen.dp.state.abstraction.Context;
import fr.ensicaen.dp.state.abstraction.State;
import fr.ensicaen.dp.state.abstraction.Storage;
import fr.ensicaen.dp.state.implementation.Mail;
import fr.ensicaen.dp.state.implementation.MailStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Connected implements State {

    private static Logger logger = LoggerFactory.getLogger(Connected.class);
    private static String STATE = "Connected !";
	private Storage storage;

	public Connected(String username, String password) {
		this.storage = new MailStorage();
		logger.info(this.toString());
	}

	public void user(Context c, String username) {
		logger.info("Already connected !");
	}

	public void pass(Context c, String password) {
		logger.info("Already connected !");
	}

	public int list(Context c, int messageNumber) {
		
		if(messageNumber < storage.getMails().size()){
			return storage.getMails().get(messageNumber).getSize();
		}
		
		return storage.getMails().size();
	}

	public Mail retrieve(Context c, int messageNumber) {
		if(messageNumber < storage.getMails().size()){
			return storage.getMail(messageNumber);
		}
		
		return null;
	}
	
	public List<Mail> retrieve(Context c) {
		return storage.getMails();
	}

	public void quit(Context c) {
		c.setState(new WaitUsername());
	}
	
	public String toString(){
		return STATE;
	}
}
