package fr.ensicaen.dp.state.implementation;


import java.util.ArrayList;

import fr.ensicaen.dp.state.abstraction.Storage;

/* This class is used like a MOCK */

public class MailStorage implements Storage {
	
	private ArrayList<Mail> mails;
	
	public MailStorage(){
		mails = new ArrayList<Mail>();
		
		mails.add(new Mail("Mail 1"));
		mails.add(new Mail("Mail 2"));
		mails.add(new Mail("Mail 3"));
		mails.add(new Mail("Mail 4"));
	}

	public ArrayList<Mail> getMails() {
		return mails;
	}

	public Mail getMail(int index) {
		return mails.get(index);
	}

}
