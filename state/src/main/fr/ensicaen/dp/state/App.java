package fr.ensicaen.dp.state;

import fr.ensicaen.dp.state.implementation.Client;
import fr.ensicaen.dp.state.implementation.SPOP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

	private static Logger logger = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		
		SPOP server = new SPOP();
		Client c = new Client();
		server.addClient(c);
		
		c.user("Romain");
		
		c.retrieve();
		
		c.pass("MDP");
		
		logger.info(c.retrieve(0).toString());
		
		c.quit();
		
	}

}
