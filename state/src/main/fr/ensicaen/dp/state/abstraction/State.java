package fr.ensicaen.dp.state.abstraction;

import fr.ensicaen.dp.state.implementation.Mail;

import java.util.List;

public interface State {
	
	void user(Context c, String username);
	void pass(Context c, String password);
	int list(Context c, int messageNumber);
	Mail retrieve(Context c, int messageNumber);
	List<Mail> retrieve(Context c);
	void quit(Context c);

}
