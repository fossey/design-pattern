package fr.ensicaen.dp.state.abstraction;

import fr.ensicaen.dp.state.implementation.Mail;

import java.util.List;

public interface Storage {
	
	List<Mail> getMails();
	Mail getMail(int index);
}
