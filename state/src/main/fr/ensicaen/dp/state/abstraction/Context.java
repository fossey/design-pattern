package fr.ensicaen.dp.state.abstraction;

import fr.ensicaen.dp.state.implementation.Mail;

import java.util.List;

public interface Context {
	
	void setState(State s);
	void user(String username);
	void pass(String password);
	List<Mail> retrieve();
	Mail retrieve(int messageNumber);
	void quit();
	
}
