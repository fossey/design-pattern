package fr.ensicaen.dp.strategy;

import fr.ensicaen.dp.strategy.abstraction.ValidationException;
import fr.ensicaen.dp.strategy.implementation.PhoneValidator;
import fr.ensicaen.dp.strategy.implementation.TextInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {

        TextInput cli = new TextInput(new PhoneValidator());

        String input = "0631631755";
        logger.info("Validate : {}", input);
        try {

            cli.validate(input);

            logger.info("Valid !");

            input = "---ç_èçè--";
            logger.info("Validate : {}", input);
            cli.validate(input);
            logger.info("Valid !");

        } catch (ValidationException e) {
            logger.error("Invalid !");
        }

    }

}
