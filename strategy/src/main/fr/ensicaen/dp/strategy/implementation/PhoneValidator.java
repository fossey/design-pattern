package fr.ensicaen.dp.strategy.implementation;

import fr.ensicaen.dp.strategy.abstraction.ValidationException;
import fr.ensicaen.dp.strategy.abstraction.ValidationStrategy;

public class PhoneValidator implements ValidationStrategy {

	public void validate(String element) throws ValidationException {
		
		if (element.matches("\\d{10}") || 
			element.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}") ||
			element.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}") ||
			element.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")){

			return;
		}
		
		throw new ValidationException(element + " is not formatted as phone number");
	}

}
