package fr.ensicaen.dp.strategy.implementation;

import fr.ensicaen.dp.strategy.abstraction.ValidationException;
import fr.ensicaen.dp.strategy.abstraction.ValidationStrategy;

public class TextInput {
	
	private ValidationStrategy validator;
	
	public TextInput(ValidationStrategy validatorImplementation){
		validator = validatorImplementation;
	}
	
	public void validate(String input) throws ValidationException {
		validator.validate(input);
	}
	
	public void setValidator(ValidationStrategy validator){
		this.validator = validator;
	}

}
