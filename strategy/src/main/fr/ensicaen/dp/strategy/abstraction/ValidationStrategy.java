package fr.ensicaen.dp.strategy.abstraction;

public interface ValidationStrategy {
	void validate(String element) throws ValidationException;
}
