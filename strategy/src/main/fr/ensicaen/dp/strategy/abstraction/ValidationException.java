package fr.ensicaen.dp.strategy.abstraction;

@SuppressWarnings("serial")
public class ValidationException extends Exception {
	
	public ValidationException(String message){
		super(message);
	}

}
