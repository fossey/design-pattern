package fr.ensicaen.dp.adaptor.implementation;

import fr.ensicaen.dp.adaptor.abstraction.IAdaptor;

public class InternalFormatter {

    private IAdaptor adaptor;

    public InternalFormatter(IAdaptor adaptor) {
        this.adaptor = adaptor;
    }

    public void formatString(String data, String format) {
        adaptor.formatString(data, format);
    }

}
