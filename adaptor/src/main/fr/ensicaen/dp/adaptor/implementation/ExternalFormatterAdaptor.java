package fr.ensicaen.dp.adaptor.implementation;

import fr.ensicaen.dp.adaptor.abstraction.IAdaptor;

public class ExternalFormatterAdaptor implements IAdaptor {

    public void formatString(String data, String format) {
        ExternalFormatter ef = new ExternalFormatter();
        ef.format(format, data);
    }

}
