package fr.ensicaen.dp.adaptor.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ExternalFormatter {

    private static Logger logger = LoggerFactory.getLogger(ExternalFormatter.class);

    void format(String format, String data) {
        logger.info("Format : {} Data : {}", format, data);
    }

}
