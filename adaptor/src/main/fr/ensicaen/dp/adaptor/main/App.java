package fr.ensicaen.dp.adaptor.main;

import fr.ensicaen.dp.adaptor.implementation.ExternalFormatterAdaptor;
import fr.ensicaen.dp.adaptor.implementation.InternalFormatter;

public class App {

    public static void main(String[] args) {
        InternalFormatter p = new InternalFormatter(new ExternalFormatterAdaptor());
        p.formatString("1234", "%d");
    }

}
