package fr.ensicaen.dp.adaptor.abstraction;

public interface IAdaptor {

    void formatString(String data, String format);

}
